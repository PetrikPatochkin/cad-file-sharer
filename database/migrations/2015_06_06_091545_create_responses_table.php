<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('responses', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('creator_id');
            $table->integer('to_user_id');
            $table->tinyInteger('status')->default(0);
            /*
             * 0 - neutral(just inform user about smth)
             * 1 - denied(denied requested files)
             * 2 - approved(requested files was approved)
             */
            $table->text('text');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('responses', function(Blueprint $table)
		{
			//
		});
	}

}
