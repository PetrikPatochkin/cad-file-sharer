<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create(['email'=>'igor@rambler.ru','password'=>Hash::make('123'),'name'=>'Igor']);
        User::create(['email'=>'o.tsoorin@gmail.com','password'=>Hash::make('123'),'name'=>'Oleg Tsoorin','role'=>'admin']);
    }

}