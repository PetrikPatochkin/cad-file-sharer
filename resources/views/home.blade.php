@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<div class="row">
                        <div class="col-md-12 text-center">
                        @if(\Auth::user()->active_status == 0)
                            @if ((isset($files[1])) && (isset($files[2])) && (isset($files[3])))
                                <div class="alert alert-warning" role="alert">
                                    <h2>You successfully download all files</h2>
                                    <p>Please wait patiantly when your teacher approve it</p>
                                </div>
                            @else
                                <div class="alert alert-warning" role="alert">
                                    <h2>Download all your files please</h2>
                                </div>
                            @endif
                        @elseif(\Auth::user()->active_status == 1)
                                <div class="alert alert-danger" role="alert">
                                    <h2>Your current files were denied</h2>
                                    <p>Read first response in list to understand why. Than load required files</p>
                                </div>
                        @elseif(\Auth::user()->active_status == 2)
                                <div class="alert alert-success" role="alert">
                                    <h2>Your files were approved</h2>
                                    <p>Don't load any other files, because your status would be reset</p>
                                </div>
                        @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your files.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/update') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row form-group">
                                    <label class="col-md-6 control-label">Текст диплома (після підпису керівника, у PDF, обсяг не більше 6 Мб.)</label>
                                    <div class="col-md-4 input-row">
                                        @if(isset($files[1]))
                                            <a class="btn" href="/file?id={{ $files[1]['id'] }}" target="_blank">
                                                {{ $files[1]['name'] }}
                                            </a>
                                            <a href="#" class="remove-file" data-id="1">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        @else
                                            <input type="file" class="form-control" name="file1">
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-6 control-label">Анотація (укр.,рос.,англ., у PDF, обсяг не більше 6 Мб.)</label>
                                    <div class="col-md-4 input-row">
                                        @if(isset($files[2]))
                                            <a class="btn" href="/file?id={{ $files[2]['id'] }}" target="_blank">
                                                {{ $files[2]['name'] }}
                                            </a>
                                            <a href="#" class="remove-file" data-id="2">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        @else
                                            <input type="file" class="form-control" name="file2">
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-6 control-label">Додаткова інформація (у PDF, обсяг не більше 6 Мб.)</label>
                                    <div class="col-md-4 input-row">
                                        @if(isset($files[3]))
                                            <a class="btn" href="/file?id={{ $files[3]['id'] }}" target="_blank">
                                                {{ $files[3]['name'] }}
                                            </a>
                                            <a href="#" class="remove-file" data-id="3">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        @else
                                            <input type="file" class="form-control" name="file3">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if (isset($responses) && !empty($responses))
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Your responses history</h4>
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Text</th>
                                        <th>Reviewed by</th>
                                        <th>Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($responses as $response)
                                        <tr class="<?php
                                        switch ($response['status']){
                                            case 0:
                                                echo "active";
                                                break;
                                            case 1:
                                                echo "danger";
                                                break;
                                            case 2:
                                                echo "success";
                                                break;
                                        }
                                        ?>">
                                            <td>
                                                @if($response['status'] == 0)
                                                    Neutral
                                                @elseif($response['status'] == 1)
                                                    Denied
                                                @elseif($response['status'] == 2)
                                                    Approved
                                                @endif
                                            </td>
                                            <td>{{ $response['text'] }}</td>
                                            <td>{{ $response['creator']['name'] }}</td>
                                            <td>
                                                {{ (!is_null($response['created_at']))?
                                                    Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$response['created_at'])
                                                    ->format('j-M-y H:i'):'' }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script src="/js/load.js"></script>
@endsection