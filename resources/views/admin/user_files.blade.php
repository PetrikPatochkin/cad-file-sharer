@extends('admin.template')

@section('inner-content')
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert <?php
                switch($user->active_status){
                    case 0:
                        echo "alert-warning";
                        break;
                    case 1:
                        echo "alert-danger";
                        break;
                    case 2:
                        echo "alert-success";
                        break;
                }
            ?>" role="alert">
                <h2>{{ $user->name }}</h2>
                <p>Files from user <b>{{ $user->name }}</b><a href="{{ route('user-edit',['id'=>Request::get('id')]) }}"> <span class="glyphicon glyphicon-edit">Edit</span></a></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row form-group">
                <label class="col-md-6 control-label">Текст диплома (після підпису керівника, у PDF, обсяг не більше 6 Мб.)</label>
                <div class="col-md-4 input-row">
                    @if(isset($files[1]))
                        <a class="btn" href="/file?id={{ $files[1]['id'] }}" target="_blank">
                            {{ $files[1]['name'] }}
                        </a>
                    @else
                        <div class="btn">Not added</div>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-6 control-label">Анотація (укр.,рос.,англ., у PDF, обсяг не більше 6 Мб.)</label>
                <div class="col-md-4 input-row">
                    @if(isset($files[2]))
                        <a class="btn" href="/file?id={{ $files[2]['id'] }}" target="_blank">
                            {{ $files[2]['name'] }}
                        </a>
                    @else
                        <div class="btn">Not added</div>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-6 control-label">Додаткова інформація (у PDF, обсяг не більше 6 Мб.)</label>
                <div class="col-md-4 input-row">
                    @if(isset($files[3]))
                        <a class="btn" href="/file?id={{ $files[3]['id'] }}" target="_blank">
                            {{ $files[3]['name'] }}
                        </a>
                    @else
                        <div class="btn">Not added</div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 col-md-offset-4">
                    <a href="/">
                        <button type="button" class="btn btn-danger">Back</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3>
                Create response for this user
            </h3>
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/admin/send_response') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                <div class="form-group">
                    <label for="text" class="col-md-4 control-label">Message status</label>
                    <div class="col-md-6 inpur-row">
                        <select class="form-control" id="response_status" name="status">
                            <option value="0">Just message</option>
                            <option value="1" style="background-color: #d9534f;color:#fff">Deny</option>
                            <option value="2" style="background-color: #5cb85c;color:#fff">Approve</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Text</label>
                    <div class="col-md-6 inpur-row">
                        <textarea name="text" id="text" rows="7" style="width:100%"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (isset($responses) && !empty($responses))
    <div class="row">
        <div class="col-md-12">
            <h4>Responses history</h4>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Text</th>
                        <th>Reviewed by</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($responses as $response)
                    <tr class="<?php
                        switch ($response['status']){
                            case 0:
                                echo "active";
                                break;
                            case 1:
                                echo "danger";
                                break;
                            case 2:
                                echo "success";
                                break;
                        }
                    ?>">
                        <td>
                            @if($response['status'] == 0)
                                Neutral
                            @elseif($response['status'] == 1)
                                Denied
                            @elseif($response['status'] == 2)
                                Approved
                            @endif
                        </td>
                        <td>{{ $response['text'] }}</td>
                        <td>{{ $response['creator']['name'] }}</td>
                        <td>{{ (!is_null($response['created_at']))?
                            Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$response['created_at'])
                            ->format('j-M-y H:i'):'' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
@endsection