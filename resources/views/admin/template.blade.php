@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Administrative panel
                    <a class="btn btn-info btn-sm pull-right" href="{{ route('user-edit',['id'=>\Auth::user()->id]) }}">My profiler</a>
                    <a class="btn btn-success btn-sm pull-right" href="/admin">Users list</a>
                    <a class="btn btn-danger btn-sm pull-right" href="{{ route('user-create') }}">Create user</a>
                </div>

                <div class="panel-body">
                    @yield('inner-content')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection