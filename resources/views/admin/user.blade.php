@extends('admin.template')

@section('inner-content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('user-edit',['id'=>Request::get('id')]) }}" method="POST" role="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-md-4 control-label">Name</label>
                    <div class="col-md-6 input-row">
                        <input type="text" class="form-control" name="name" value="{{ $user->name }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Email</label>
                    <div class="col-md-6 input-row">
                        <input type="email" class="form-control" name="email" value="{{ $user->email }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Password</label>
                    <div class="col-md-6 input-row">
                        <input type="password" class="form-control" name="password" placeholder="Don't fill to prevent change"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <div class="col-md-2">
                        <a href="/admin/user/files?id={{ $user->id }}">
                            <button type="button" class="btn btn-danger">Close</button>
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-info" href="#"
                                onclick="return confirm('Do you really want to delete this user?')?
                                        window.location='{{ route('user-delete',['id'=>$user->id]) }}':false;
                                        ">
                            Delete this user
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection