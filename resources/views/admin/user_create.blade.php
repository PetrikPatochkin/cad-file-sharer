@extends('admin.template')

@section('inner-content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <h2>Creating user</h2>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('user-edit') }}" method="POST" role="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-md-4 control-label">Name</label>
                    <div class="col-md-6 input-row">
                        <input type="text" class="form-control" name="name" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Email</label>
                    <div class="col-md-6 input-row">
                        <input type="email" class="form-control" name="email" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Password</label>
                    <div class="col-md-6 input-row">
                        <input type="password" class="form-control" name="password"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                    <div class="col-md-3">
                        <a href="/admin">
                            <button type="button" class="btn btn-danger">Close</button>
                        </a>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection