@extends('admin.template')

@section('inner-content')
    <div class="row">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>E-mail</th>
                <th>Status</th>
                <th>Last update</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="<?php
                switch ($user->active_status){
                    case 0:
                        echo "active";
                        break;
                    case 1:
                        echo "danger";
                        break;
                    case 2:
                        echo "success";
                        break;
                }
                ?>">
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if($user->active_status == 0)
                            Not checked
                        @elseif($user->active_status == 1)
                            Denied
                        @elseif($user->active_status == 2)
                            Approved
                        @endif
                    </td>
                    <td>{{ (!is_null($user->last_updated))?
                            Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->last_updated)
                            ->format('j-M-y H:i'):'' }}</td>
                    <td>
                        <a href="/admin/user/files?id={{ $user->id }}">
                            <div class="btn btn-info btn-xs">Show user files</div>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection