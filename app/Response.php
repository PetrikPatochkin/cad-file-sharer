<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'responses';

    protected $fillable = ['creator_id', 'to_user_id', 'status', 'text'];

    public function creator(){
        return $this->hasOne('App\User','id','creator_id');
    }
}