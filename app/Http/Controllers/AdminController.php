<?php namespace App\Http\Controllers;

use App\User;
use App\File;
use App\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use Request;

class AdminController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Admin Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your admin panel
    | for administrators for users' files monitoring
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the list of users(students)
     *
     * @return Response
     */
    public function index()
    {
        $users = User::select(\DB::raw('users.*, MAX(files.updated_at) as last_updated '))
            ->where('role','=','user')
            ->leftJoin('files','users.id','=','files.user_id')
            ->groupBy('users.id')
            ->orderBy('active_status','ASC')
            ->orderBy('last_updated','DESC')->get();

        return view('admin.index')->withUsers($users);
    }

    public function user_files()
    {
        $id = Request::get('id');
        $files = File::where('user_id','=',$id)->get()->toArray();
        $responses = Response::with('creator')->where('to_user_id','=',$id)->orderBy('created_at','DESC')->get()->toArray();

        $result = [];
        foreach($files as $file){
            $result[$file['type']] = $file;
        }
        $user = User::find($id);
        return view('admin.user_files')->withUser($user)->withFiles($result)->withResponses($responses);
    }

    public function user()
    {
        $id = Request::get('id');
        $user = User::find($id);

        return view('admin.user')->withUser($user);
    }

    public function create_user()
    {
        return view('admin.user_create');
    }

    public function save_user(\Illuminate\Http\Request $request)
    {
        $id = Request::get('id');
        $this->validate($request,[
            'email' => 'required|email|unique:users,email,'.$id,
            'name' => 'required|min:4',
            'password' => 'min:6'. ((!$id)?'|required':''),
        ]);

        $data = Request::only('email','name','password');
        if($data['password']){
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }

        $user = User::firstOrNew(['id'=>$id]);
        $user->fill($data);
        $user->save();

        return redirect()->route('user-files',['id'=>$user->id]);
    }

    public function delete_user(){
        $id = Request::get('id');
        if(is_dir(storage_path().'/app/'.$id))
            File::deleteDir(storage_path().'/app/'.$id);

        File::where('user_id','=',$id)->delete();
        Response::where('to_user_id','=',$id)->delete();
        User::destroy($id);

        return redirect()->to('/admin');
    }

    public function send_response(\Illuminate\Http\Request $request){
        $v = \Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'status' => 'required|integer|in:0,1,2'
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $params = Request::only('user_id','status','text');

        Response::create([
            'creator_id' => \Auth::user()->id,
            'to_user_id' => $params['user_id'],
            'status' => $params['status'],
            'text' => $params['text']
        ]);

        if($params['status'] == 1 || $params['status'] == 2){
            $user = User::find($params['user_id']);
            $user->active_status = $params['status'];
            $user->save();
        }

        return redirect()->back();
    }

}