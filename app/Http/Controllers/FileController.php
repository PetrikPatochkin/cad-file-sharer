<?php namespace App\Http\Controllers;

use App\User;
use App\File;
use Auth;
use Request;
use Response;

class FileController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function get()
    {
        $file_id = Request::get('id');
        $file = File::find($file_id);
        if($file->user_id === Auth::user()->id || Auth::user()->role === 'admin')
            return Response::download(storage_path().'/app/'.$file->path, $file->name);
        else
            return 'You have no power here';
    }

}
