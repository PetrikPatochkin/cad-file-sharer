<?php namespace App\Http\Controllers;

use App\User;
use App\File;
use App\Response;
use Auth;
use Request;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('user');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $files = File::where('user_id','=',Auth::user()->id)->get()->toArray();
        $responses = Response::with('creator')
            ->where('to_user_id','=',\Auth::user()->id)
            ->orderBy('created_at','DESC')->get()->toArray();
        $result = [];
        foreach($files as $file){
            $result[$file['type']] = $file;
        }
		return view('home')->withFiles($result)->withResponses($responses);
	}

    public function update(\Illuminate\Http\Request $request)
    {
        $v = \Validator::make($request->all(), [
            'file1' => 'max:6000|mimes:pdf',
            'file2' => 'max:6000|mimes:pdf',
            'file3' => 'max:6000|mimes:pdf'
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        for($i=1; $i<=3; $i++){
            if(Request::hasFile('file'.$i)){
                $file = Request::file('file'.$i);
                if($file->isValid()){
                    $store_file_name = time() . '-' . md5($file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
                    $store_file_path = Auth::user()->id . '/' . $store_file_name;

                    if(!is_dir(storage_path().'/app/'.Auth::user()->id))
                        mkdir(storage_path().'/app/'.Auth::user()->id);
                    $file->move(storage_path().'/app/'.Auth::user()->id, $store_file_name );

                    $file_obj = File::firstOrNew([
                        'user_id' => Auth::user()->id,
                        'type' => $i
                    ]);
                    // Remove previous file
                    if($file_obj->id && $file_obj->path){
                        unlink(storage_path().'/app/' . $file_obj->path);
                    }
                    $file_obj->name = $file->getClientOriginalName();
                    $file_obj->path = $store_file_path;
                    $file_obj->status = 0;
                    $file_obj->save();
                    // Update status of the user
                    if(!isset($can_update_status_to_0)) $can_update_status_to_0 = true;
                    if($can_update_status_to_0){
                        Auth::user()->active_status = 0;
                        Auth::user()->save();
                        $can_update_status_to_0 = false;
                    }
                }
            }
        }
        return redirect()->back();
    }

}
