<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('home', function(){
    return Redirect::to('/');
});
Route::get('/', 'HomeController@index');
Route::get('/file', 'FileController@get');
Route::post('update', 'HomeController@update');

Route::get('admin', 'AdminController@index');
Route::get('admin/user/files', ['uses'=>'AdminController@user_files','as'=>'user-files']);
Route::get('admin/user/create', ['uses'=>'AdminController@create_user', 'as'=>'user-create']);
Route::get('admin/user/delete', ['uses'=>'AdminController@delete_user', 'as'=>'user-delete']);
Route::get('admin/user', ['uses'=>'AdminController@user','as'=>'user-edit']);
Route::post('admin/user', 'AdminController@save_user');

Route::post('admin/send_response', 'AdminController@send_response');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
