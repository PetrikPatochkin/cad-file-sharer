<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'files';

    protected $fillable = ['name','type','user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function deleteDir($dir) {
        $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it,
            \RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }
}